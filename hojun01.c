#include <stdio.h>
#include <stdlib.h>

#define MAX_LEN		32
#define BYTE		8

void display(unsigned int row, unsigned short bitCnt) {
	unsigned char i;
	for(i=1; i<=bitCnt; i++) {
		printf("%d", (row >> (bitCnt - i)) & 1);
		if(!(i % 4)) printf(" ");
	}
}

void divider(unsigned int *a, unsigned char b) {
	unsigned char i, j = b / 4, k = 32 / b, l;
	unsigned int div = 0;
	for(i=0; i<j; i++) {
		div <<= 4;
		div += 0xF;
	}
	for(i=0; i<MAX_LEN; i++) {
		printf("a[%d]\n\n", i);
		for(l=0; l<k; l++) display((a[i]&(div<<b*l))>>b*l, b);
		printf("\n");
	}
}

void divider24(unsigned int *a, unsigned char b) {
	float len = MAX_LEN * sizeof(unsigned int);

	unsigned char i, j, k = b / BYTE;
	if(len / (b / BYTE) > (int)(len / (b / BYTE))) j = (len / k) + 1;
	else j = len / k;

	for(i=0; i<j; i++) {
		printf("(a+(i*k) (a+(%d*%d) \n", i, k);
		display(*(int *)((char *)a+(i*k))&0xFFFFFF, b);
	}
}

unsigned int mask(unsigned char bitCount) {
	unsigned char i = 0;
	char rest = bitCount;
	unsigned int div = 0;

	for(i=0; i<8; i++) {
		div <<= 4;
		if(rest - 4 >= 0) {
			rest -= 4;
			div += 0xF;
		}
	}
	return div;
}

void divider32(unsigned int *a, unsigned char b, unsigned int *c) {
	unsigned char count = b / 4;
	unsigned char rest = count;
	unsigned char i, j, k = 0;
	unsigned int buf = 0;
	unsigned int cLen = 0;

	for(i=0; i<128; i++) {
		unsigned int mask = 0xF;
		for(j=0; j<2; j++) {
			buf |= (*a & mask) << 8 * k;
			mask <<= 4;
			if(!--rest) {
				// display(b == 4 ? buf >> j * b : buf, b);
				if(cLen < 32) {
					c[cLen] = b == 4 ? buf >> j * b : buf;
					cLen ++;
				}
				buf = 0;
				rest = count;
				// printf("\n\n");
			}
		}
		k = buf ? k + 1 : 0;
		a = (int *)((char *)a + 1);
	}
}

void reverse(unsigned int *src, unsigned int b, unsigned int *store) {
	unsigned int i, j, k = 0;
	unsigned int length = 32;
	int remain = 32;
	// bit operations
	unsigned int l = b / 4;
	unsigned int div = 0;
	for(i=0; i<l; i++) {
		div <<= 4;
		div += 0xF;
	}

	// add
	for(i=0; i<32; i++) {
		for(j=0; remain>0; j++) {
			// printf("k[%2d] j[%2d] 0x%x\n", k, j, div);
			store[i] += (src[k] & div) << b * j;
			remain -= b;
			k ++;
			if(k > 32) return ;
		}
		remain = length;
	}

	// ptr move
}

int main(int argc, char *argv[]) {
	unsigned char b = atoi(argv[1]);

	unsigned int a[MAX_LEN];
	unsigned int c[MAX_LEN];
	unsigned int e[MAX_LEN] = {0, };
	unsigned char i;
	// unsigned int d[3];
	unsigned int d[3] = {0x12345678, 0x11112222, 0x33334444};

	// printf("input data 3 : ");
	// scanf("%x %x %x", d, &d[1], &d[2]);
	// scanf("%x", d);

	// for(i=0; i<MAX_LEN; i++) a[i] = 0xAAAAAAAA;
	// for(i=0; i<MAX_LEN; i++) a[i] = 0x6EC8F731;
	for(i=0; i<MAX_LEN; i++) a[i] = d[i % 3];
	for(i=0; i<32; i++) printf("a[%d] = %x\n", i, a[i]);
	printf("\n");

	for(i=0; i<3; i++) {
		display(a[i], 32);
		printf("\n");
	}
	// display(0x6EC8F731, 32);
	printf("\n\n");

	// if(b == 24) divider24(a, b);
	// else divider(a, b);

	divider32(a, b, c);

	for(i=0; i<MAX_LEN; i++) {
		printf("c[%2d] = ", i);
		display(c[i], b);
		printf("\n");
	}

	reverse(c, b, e);

	for(i=0; i<MAX_LEN; i++) {
		printf("e[%2d] = ", i);
		display(e[i], 32);
		printf("\n");
	}

	return 0;
}