#include <stdio.h>
#include <stdlib.h>

#define MAX_LEN		32
#define BYTE		8

void display(int row, int bitCnt) {
	int i;
	for(i=1; i<=bitCnt; i++) {
		printf("%d", (row >> (bitCnt - i)) & 1);
		if(!(i % 4)) printf(" ");
	}
}

void divider32(int *a, int b, int *c) {
	// count는 몇 비트로 나눌 것이지에 따라서 정해지는 값인데,
	// 4, 8, 16, 24, 32 비트는 4의 배수이므로 이 값에 따라 버퍼를 채운다.

	// rest는 count 값을 실제적으로 사용하는 변수이다.
	// 예를 들어 4비트로 나눌 것이라면 b가 4이고, 그러면 count와 rest는 b / 4인 1이 된다.

	// divider32가 4비트씩 마스킹 한다(즉 버퍼에 4비트씩 데이터를 채운다)
	// 이때 한 번 버퍼에 4비트를 채우면 rest가 1씩 줄어드는 형태로 짜여져 있다.
	// 따라서 4비트로 나누려고 했을 때 rest가 1이므로 4비트만 버퍼에 채우고 rest는 0이 되어서 버퍼는 더 이상 값을 넣지 않고 다음으로 넘어간다.
	// 이런식으로 4, 8, 16, 24, 32 모두 처리한다.
	// (확실히 한 함수에서 다 하기보다 divider, divider24로 나눠져 처리하는 게 낫다고 본다. 한 함수에서 다 하려면 코드가 굉장히 복잡해 진다.)

	// i, j, k는 일반 제어를 위한 변수이고 buf는 버퍼, cLen은 c의 인덱스 역할 변수다.
	int count = b / 4, rest = count, i, j, k = 0, buf = 0, cLen = 0;

	// 1024라는 고정된 배열 길이에서, char 포인터로 1바이트씩 이동할 것이므로 1024 / 8 = 128이라는 반복 제어가 필요하다.
	for(i=0; i<128; i++) {
		// LSB부터 0xF씩 마스킹하면서 올라 갈 마스크 변수.
		int mask = 0xF;
		// 전체적으로 1바이트씩 움직이지만 우리는 4비트씩 버퍼에 담아야 하므로 바이트 단위 이동 내에서 4비트 단위로 끊어줄 반복문을 만들었다.
		for(j=0; j<2; j++) {
			// 버퍼에 a배열의 값(a 배열은 i 포문이 돌 때마다 바이트 단위로 이동하기 때문에 i 포문이 돌때마다 참조하는 값이 전체적으로 한 바이트 씩 이동한다)과 mask 값의 비트 연산.
			// 이때 8비트 보다 높은 비트를 분할한다면, 처음 8비트는 0~7번째에 있지만 그 다음 비트들은 8~ 형태로 되어야 한다. 따라서 8비트보다 높은 수를 연산할 때마다
			// 증가하는 k를 가지고 왼쪽 쉬프트 연산 해준다(즉, 8비트까지는 k가 0인데 그보다 높은 비트로 분할하면 8 x 1(k=1이므로), 8 x 2, ... 같은 쉬프트 연산이 일어난다.
			buf |= (*a & mask) << 8 * k;
			// 이 mask는 이 포문 안에서만 쉬프트 되는데, 즉 i 포문이 시작할 때마다 mask는 0xF로 초기화되고, 처음 0~3비트가 마스크 되어 버퍼에 담긴 후 다시 j 포문의 마지막을 돌면서
			// 다음 4비트를 4~7비트 공간에 넣기 위한 마스크 값 왼쪽 쉬프트이다.
			mask <<= 4;
			// 4비트씩 버퍼에 담은 후엔 rest를 1씩 감소한다.
			if(!--rest) {
				// 정수형 32 길이의 c 배열에 값을 담기 위한 것이다.
				if(cLen < 32) {
					// 4비트 단위로 분할할 때, 처음 4비트는 제대로 LSB부터인 0~3비트 영역으로 잘 들어간다.
					// 그러나 그 다음 4~7비트는 그대로 두면 4~7비트로 들어가기 때문에 c 배열 내에서 4~7비트에 4비트가 존재하는 문제가 발생한다.
					// 따라서 하는 수 없이 식이 지저분해지지만 우측 쉬프트 하여 0~3번 비트로 두었다.
					c[cLen] = b == 4 ? buf >> j * b : buf;
					// 배열 인덱스 조정
					cLen ++;
				}
				// 버퍼의 값을 c 배열에 썼으면 비운다.
				buf = 0;
				// 4비트씩 버퍼에 채워 줄 rest 값도 다시 count 값으로 초기화 해준다.
				rest = count;
			}
		}
		// 버퍼의 값이 있다는 건 아직 해당 비트 분할이 덜 끝났다는 것이므로 k는 증가하고 그렇지 않으면 k를 초기화해서 j 포문 안의 왼쪽 쉬프트 연산을 처음으로 돌린다.
		k = buf ? k + 1 : 0;
		// 배열 a를 한 바이트씩 옮기고 그 영역을 다시 32비트 길이로 바꿔준다.
		a = (int *)((char *)a + 1);
	}
}

void reverse(int *src, int b, int *store) {
	int i, j, k = 0, l = b / 4, length = MAX_LEN, remain = MAX_LEN, div = 0;
	for(i=0; i<l; i++) {
		div <<= 4;
		div += 0xF;
	}

	for(i=0; i<MAX_LEN; i++) {
		for(j=0; remain>0; j++) {
			store[i] += (src[k] & div) << b * j;
			remain -= b;
			k ++;
			if(k > MAX_LEN) return ;
		}
		remain = length;
	}
}

int main(void) {
	int b, i;

	int a[MAX_LEN], c[MAX_LEN], e[MAX_LEN] = {0, }, d[3] = {0x12345678, 0x11112222, 0x33334444};

	printf("input number to divide bit : ");
	scanf("%d", &b);

	for(i=0; i<MAX_LEN; i++) a[i] = d[i % 3];
	for(i=0; i<32; i++) printf("a[%2d] = %x\n", i, a[i]);
	printf("\n");

	for(i=0; i<3; i++) {
		display(a[i], 32);
		printf("\n");
	}
	printf("\n\n");

	divider32(a, b, c);

	for(i=0; i<MAX_LEN; i++) {
		printf("c[%2d] = ", i);
		display(c[i], b);
		printf("\n");
	}
	printf("\n\n");

	reverse(c, b, e);

	for(i=0; i<MAX_LEN; i++) {
		printf("e[%2d] = ", i);
		display(e[i], 32);
		printf("\n");
	}
	printf("\n\n");

	return 0;
}
