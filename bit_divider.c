#include <termios.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX_LEN			32
#define BYTE			8
#define BIT_INIT_CNT	10

#define DISPLAY 							"---------------------------------------------------------\n"\
											"- Bit Divider v0.2                              \t-\n"\
											"---------------------------------------------------------\n"
#define	DISPLAY_E							"---------------------------------------------------------\n"
#define HELP_D 								"*****Help Key Functions\n"											
#define LIST_D 								"*****Bit Bucket List\n"	


#define HELP_U		72
#define HELP 		104	
#define CREAT_U		67
#define CREAT		99
#define	LIST_U		76
#define LIST		108
#define	DELETE_U	68
#define	DELETE 		100
#define	MODIFY_U	77
#define	MODIFY 		109
#define	INSERT_U	73
#define	INSERT 		105
#define	INFO_U		69
#define	INFO 		101

// bit_t 구조체로 사용자가 만든 비트를 담은 배열이라고 보면 됨
struct bits_t {
	void *bit;		// 비트를 담기 위한 배열
	unsigned int length;	// 담을 수 있는 길이를 나타내는 변수
} typedef bit_t;

// bucket의 구조체
struct bitBuckets_t {
	bit_t **bits_t;			// bit_t 구조체들을 여럿 갖고 있을 bit_t 이중 포인터(이중 배열이라 보면 됨)
	unsigned int bitCount;		// 현재 bit_t 구조체들이 얼마나 만들어져 이중 배열에 채워져 있는지를 나타내는 변수
	unsigned int bucketLength;	// 현재 이중 배열이 최대로 갖고 있을 수 있는 크기를 나타냄
} typedef bitBucket_t;

// 모든 bucket과 그 관련 정보를 갖고 있을 전역 변수
static bitBucket_t bitBucket = {0, 0, BIT_INIT_CNT};

int getch(void) {
	int ch;
	struct termios buf;
	struct termios save;
	tcgetattr(0, &save);

	buf = save;
	buf.c_lflag &= ~(ICANON|ECHO);
	buf.c_cc[VMIN] = 1;
	buf.c_cc[VTIME] = 0;
	tcsetattr(0, TCSAFLUSH, &buf);
	ch = getchar();
	tcsetattr(0, TCSAFLUSH, &save);
	return ch;
}

void init() {
	printf("%s\n", DISPLAY);
	printf("bitBucket inicializing ... count : %d\n", BIT_INIT_CNT);
	bitBucket.bits_t = (bit_t **)calloc(BIT_INIT_CNT, sizeof(bit_t));
	perror("bitBucket inicialized ... ");
	
}

void printHelp() {
	printf("%s%s\'Bit Bucket\' \nCreate key 'C'\nList Key 'L'\nDelete Key 'D'\nModify Key 'M'\nInserton Key 'I'\nInformation Key 'E'\n%s\
		", DISPLAY_E, HELP_D, DISPLAY_E);
}

void modifyBucket(unsigned int count) {
	// TODO realloc -temp to bucket

	// realloc으로 이중 배열의 크기를 조정한다.
	bitBucket.bits_t = (bit_t **)realloc(bitBucket.bits_t, sizeof(bit_t) * (count + bitBucket.bucketLength));
	printf("bitBucket modified ... ");
	// bucketLength를 갱신한다.
	bitBucket.bucketLength = count + bitBucket.bucketLength;
	printf("bitCount : %d, bucketLength : %d\n", bitBucket.bitCount, bitBucket.bucketLength);
}

// bit를 담을 bucket을 만드는 함수
void createBit(unsigned int count, unsigned int length, unsigned int byte) {
	unsigned short i;

	// 만약 사용자가 입력한 bucket 수와 현재 bitBucket의 bit_t 구조체 이중 배열을 합친 길이가
	// 현재 bitBucket의 bit_t 구조체 이중 배열의 크기보다 크면 이중 배열의 크기를 더 늘려줘야 한다.
	if(count + bitBucket.bitCount > bitBucket.bucketLength) modifyBucket(count + bitBucket.bitCount - bitBucket.bucketLength);

	// 각 bucket 안에 사용자가 입력한 길이만큼의 공간을 갖고 있을 bit_t를 만들어 동적으로 할당한다.
	for(i=0; i<count; i++) {
		*(bitBucket.bits_t + bitBucket.bitCount + i) = (bit_t *)malloc(sizeof(bit_t));
		(*(bitBucket.bits_t + bitBucket.bitCount + i)) -> bit = calloc(1, byte);
		(*(bitBucket.bits_t + bitBucket.bitCount + i)) -> length = length;
		printf("bitBucket.bits_t[%d] created, \t", bitBucket.bitCount + i);
		printf("bitBucket.bits_t[%d]'s length : %d\n", bitBucket.bitCount + i, (*(bitBucket.bits_t + bitBucket.bitCount + i)) -> length);
	}
	// bitCount를 갱신한다.
	bitBucket.bitCount += i;
}

void create() {
	unsigned int t1, t2;
	// 사용자로부터 bit를 담을 bucket의 수와 비트 길이를 입력 받는다.
	printf("Input the make count, length to create : ");
	scanf("%d %d", &t1, &t2);
	createBit(t1, t2, (float)t2 / BYTE > t2 / BYTE ? t2 / BYTE + 1 : t2 / BYTE);
}

void list() {
	unsigned short i;

	printf("%s", LIST_D);
	for(i=0; i<bitBucket.bitCount; i++) printf("bucket[%d]'s length = %d\n", i, (*(bitBucket.bits_t + i)) -> length);
}

// 지금은 굉장히 비효율적으로 동작하고 있다. 나중에 시간이 나면 트리를 추가하거나 해서 관리하면 더 효율적일듯.
void deleteBit(unsigned int t1) {
	unsigned short i;

	free((*(bitBucket.bits_t + t1)) -> bit);
	perror("bit delete ");
	free(*(bitBucket.bits_t + t1));
	perror("bucket delete ");

	for(i=0; i<bitBucket.bitCount-t1-1; i++) {
		*(bitBucket.bits_t + t1 + i) = *(bitBucket.bits_t + t1 + i + 1);
	}
	*(bitBucket.bits_t + t1 + i) = NULL;
	bitBucket.bitCount --;
}

void delete() {
	unsigned int t1;
	printf("Input the delete bucket index : ");
	scanf("%d", &t1);
	deleteBit(t1);
}

void modifyBit(unsigned int index, unsigned int length, unsigned int byte) {
	free((*(bitBucket.bits_t + index)) -> bit);
	(*(bitBucket.bits_t + index)) -> bit = calloc(1, byte);
	perror("bit modify ");
	(*(bitBucket.bits_t + index)) -> length = length;
}

void modify() {
	unsigned int t1, t2;
	printf("Input the modify index of bucket, it's length to be modified : ");
	scanf("%d %d", &t1, &t2);
	modifyBit(t1, t2, (float)t2 / BYTE > t2 / BYTE ? t2 / BYTE + 1 : t2 / BYTE);
}

void display(bit_t *bp, unsigned int start, unsigned int end) {
	unsigned short i;
	end = end > bp -> length ? bp -> length : end;
	for(i=0; i<end; i++) {
		printf("%d", (*(unsigned int *)(bp -> bit) >> (32 - i)) & 1);
		if(!((i + 1) % 4)) printf(" ");
		if(!((i + 1) % 32)) printf("\n");
	}
}

void insertBit(unsigned int index, unsigned int value, unsigned int length) {
	unsigned short i;
	bit_t *bp = *(bitBucket.bits_t + index);
	void *bit = bp -> bit;
	for(i=0; i<bp -> length / length; i++) *(int *)(bit + i * length) = value;
	printf("insertion complete !\n");
	
}

void insert() {
	unsigned int t1, t2, t3;
	printf("Input the index of bucket to be inserted, value(hex), bit length(max 32bit) : ");
	scanf("%d %x %d", &t1, &t2, &t3);
	insertBit(t1, t2, t3);
}

void infoBit(unsigned int index) {
	printf("bucket[%d]'s info\n", index);
	display(*(bitBucket.bits_t + index), 0, (*(bitBucket.bits_t + index)) -> length);
}

void info() {
	unsigned int t1;
	printf("Input the index of bucket to display info : ");
	scanf("%d", &t1);
	infoBit(t1);
}

int main(int argc, char* argv[]) {

	init();

	while(1) {
		printf("press command. (If you need to see help, press 'H')\n");
		switch(getch()) {
			case HELP_U	:
			case HELP 	:
				printHelp();
			break;
			case CREAT_U	:
			case CREAT 		:
				create();
				getch();
			break;
			case LIST_U		:
			case LIST 		:
				list();
			break;
			case DELETE_U	:
			case DELETE 	:
				delete();
				getch();
			break;
			case MODIFY_U	:
			case MODIFY 	:
				modify();
				getch();
			break;
			case INSERT_U	:
			case INSERT		:
				insert();
				getch();
			break;
			case INFO_U		:
			case INFO		:
				info();
				getch();
			break;
		}
		printf("\n\n");
	}

	return 0;
}
